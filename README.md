# COMP30022 IT Project

Team Name: Pong

Project Name: PongPongPong

Description: An action game with RPG elements, which is an iOS application. The base game will be Pong but with added feature such as: RPG elements (character selection) and online multiplayer. This game would be targeted at iOS users with phone running iOS 8 or later (iphone 5, iphone 6, etc)


## Team Member:
Adrian Sutanahadi	617462

Denis Thamrin		613726

Ye Ma			597013

Xin He			625282


## Installation
* This is an iOS application, make sure you have XCode7 installed on your mac
* Git pull `git clone https://hexinpeter@bitbucket.org/pongpongpong/pongpongpong-ci.git`
* Navigate into the pulled directory `cd pongpongpong-ci`
* Double click on file `PongPongPong-CI.xcodeproj` to start
* Press "Command + R" to run the game


## Unit Testing
* The tests are in the `PongPongPongTests` folder
* Press "Command + U" to run the tests


## Credits
1. Sound effects
    * http://www.freesfx.co.uk/  
    * http://www.yinxiao.com/
    * http://www.2gei.com/sound/
