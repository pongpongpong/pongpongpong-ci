import UIKit
import XCTest
import SpriteKit
@testable import PongPongPong_CI

class BrickTests: XCTestCase {
    
    func testBrickHit() {
        let initialHealth = 2
        let brick = Brick.init(position: CGPoint(x: 0, y: 0), health: initialHealth, id: 1)
        brick.hit()
        XCTAssertEqual(initialHealth - 1, brick.health)
    }
    
    func testBrickShouldNotRemove() {
        let initialHealth = 2
        let brick = Brick.init(position: CGPoint(x: 0, y: 0), health: initialHealth, id: 1)
        let node = SKSpriteNode()
        node.addChild(brick)
        brick.hit()
        XCTAssertNotNil(brick.parent)
    }
    
    func testBrickHitWithRemoval() {
        let initialHealth = 1
        let brick = Brick.init(position: CGPoint(x: 0, y: 0), health: initialHealth, id: 1)
        let node = SKSpriteNode()
        node.addChild(brick)
        brick.hit()
        XCTAssertEqual(initialHealth - 1, brick.health)
        XCTAssertNil(brick.parent)
    }    

}
