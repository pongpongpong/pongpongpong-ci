import UIKit
import XCTest
@testable import PongPongPong_CI

class GameStateTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        super.tearDown()
        
        // clear the persisted storage in User Defaults
        NSUserDefaults.standardUserDefaults().removePersistentDomainForName(NSBundle.mainBundle().bundleIdentifier!)
    }
    
    // Singleton testing
    func testSharedInstance() {
        let insOne = GameState.sharedInstance
        XCTAssert(insOne === GameState.sharedInstance)
    }
    
    // Persisted storage test
    func testSavePlayerScore() {
        let state = GameState.sharedInstance
        let storage = NSUserDefaults.standardUserDefaults()
        XCTAssertEqual(0, storage.integerForKey("playerScore"))
        state.playerScore = 100
        state.saveState()
        XCTAssertEqual(100, storage.integerForKey("playerScore"))
    }
    
    func testSaveCharacter() {
        let state = GameState.sharedInstance
        let storage = NSUserDefaults.standardUserDefaults()
        state.character = "hunter"
        state.saveState()
        XCTAssertEqual(state.character, storage.stringForKey("character"))
    }
    
    func testSaveOpponentCharacter() {
        let state = GameState.sharedInstance
        let storage = NSUserDefaults.standardUserDefaults()
        state.opponentCharacter = "wizard"
        state.saveState()
        XCTAssertEqual(state.opponentCharacter, storage.stringForKey("opponentCharacter"))
    }
    
    func testSaveAiScore() {
        let state = GameState.sharedInstance
        let storage = NSUserDefaults.standardUserDefaults()
        state.aiScore = 10
        state.saveState()
        XCTAssertEqual(state.aiScore, storage.integerForKey("aiScore"))
    }
    
}
