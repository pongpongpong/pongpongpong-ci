import UIKit
import XCTest
import SpriteKit
@testable import PongPongPong_CI

class SinglePlayerGameSceneTests: XCTestCase {
    
    func testOpponentMove() {
        let scene = SinglePlayerGameScene(size: CGSize())
        XCTAssert(!scene.opponent.hasActions())
        scene.opponentMove()
        XCTAssert(scene.opponent.hasActions())
    }    
    
}
