import UIKit
import XCTest
import SpriteKit
@testable import PongPongPong_CI

class GameSceneTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialized() {
        let scene = GameScene(size: CGSize(width: 100, height: 100))
        XCTAssertNotNil(scene.player)
        XCTAssertNotNil(scene.opponent)
        XCTAssertNotNil(scene.ball)
        XCTAssert(scene.brickList.count > 0)
    }
    
    func testPlayerCharacter() {
        let state = GameState.sharedInstance
        state.character = "hunter"
        let scene = GameScene(size: CGSize(width: 100, height: 100))
        XCTAssertEqual(0.005/Hunter.speed, scene.player.maxSpeed)
    }
    
    func testDefaultPlayerCharacterWizard() {
        let state = GameState.sharedInstance
        state.character = nil
        let scene = GameScene(size: CGSize(width: 100, height: 100))
        XCTAssertEqual(0.005/Wizard.speed, scene.player.maxSpeed)
    }
    
    func testWrongPlayerCharInputInGameState() {
        let state = GameState.sharedInstance
        state.character = "somecharacter"
        let scene = GameScene(size: CGSize(width: 100, height: 100))
        XCTAssertEqual(0.005/Wizard.speed, scene.player.maxSpeed)
    }
    
    func testOpponentPlayerCharacter() {
        let state = GameState.sharedInstance
        state.opponentCharacter = "warrior"
        let scene = GameScene(size: CGSize(width: 100, height: 100))
        XCTAssertEqual(0.005/Warrior.speed, scene.opponent.maxSpeed)
    }
    
    func testBallMovesWhenScenceInitialized() {
        let scene = GameScene(size: CGSize(width: 100, height: 100))
        XCTAssert(scene.ball.physicsBody?.velocity != CGVector(dx: 0, dy: 0))
    }
            
}
