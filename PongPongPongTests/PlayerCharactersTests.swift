import UIKit
import XCTest
@testable import PongPongPong_CI


class PlayerCharactersTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHunter() {
        XCTAssertNotNil(Hunter.speed)
        XCTAssertNotNil(Hunter.size)
        XCTAssertNotNil(Hunter.color)
    }
    
    func testWizard() {
        XCTAssertNotNil(Wizard.speed)
        XCTAssertNotNil(Wizard.size)
        XCTAssertNotNil(Wizard.color)
    }
    
    func testWarrior() {
        XCTAssertNotNil(Warrior.speed)
        XCTAssertNotNil(Warrior.size)
        XCTAssertNotNil(Warrior.color)
    }
    
    func testHunterInit() {
        let pos = CGPoint(x: 10, y: 10)
        let character = Hunter(position: pos)
        XCTAssertEqual(pos, character.position)
    }
    
    func testWizardInit() {
        let pos = CGPoint(x: 10, y: 10)
        let character = Wizard(position: pos)
        XCTAssertEqual(pos, character.position)
    }
    
    func testWarriorInit() {
        let pos = CGPoint(x: 10, y: 10)
        let character = Warrior(position: pos)
        XCTAssertEqual(pos, character.position)
    }
    
}
