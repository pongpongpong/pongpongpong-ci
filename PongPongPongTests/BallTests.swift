import UIKit
import XCTest
@testable import PongPongPong_CI

class BallTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    // Ball speed should not be changed after updateMovingSpeed, the method is used to keep the ball under constant speed
    func testBallUpdateMovingSpeed() {
        let ball = Ball.init(position: CGPoint(x: 0, y: 0))
        let initial_speed = ball.movingSpeed
        ball.updateMovingSpeed()
        XCTAssertEqual(initial_speed, ball.movingSpeed)
    }
    
    func testBallChangeSpeed() {
        let ball = Ball.init(position: CGPoint(x: 0, y: 0))
        let speed = CGFloat.init(10)
        ball.changeSpeed(speed)
        XCTAssertEqual(speed, ball.movingSpeed)
    }
    
    func testBallNotChangeSpeedWhenStill() {
        let ball = Ball.init(position: CGPoint(x: 0, y: 0))
        ball.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        ball.changeSpeed(10.0)
        XCTAssertEqual(ball.physicsBody?.velocity, CGVector(dx: 0, dy: 0))
    }
    
    func testBallPhysicsProperties() {
        let ball = Ball.init(position: CGPoint(x: 0, y: 0))
        XCTAssertEqual(0, ball.physicsBody?.friction)
        XCTAssertEqual(0, ball.physicsBody?.linearDamping)
        XCTAssertEqual(0, ball.physicsBody?.angularDamping)
        XCTAssertEqual(1, ball.physicsBody?.restitution)
    }

    
}
