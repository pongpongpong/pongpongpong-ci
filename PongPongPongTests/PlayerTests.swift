import UIKit
import XCTest
@testable import PongPongPong_CI

class PlayerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPlayerMoveTo() {
        let player = Player.init(position: CGPoint(x: 0, y: 0), maxSpeed: 10)
        player.moveTo(100)
        XCTAssert(player.hasActions())
    }

}
