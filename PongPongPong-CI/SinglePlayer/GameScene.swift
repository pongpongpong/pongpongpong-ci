import SpriteKit
import AVFoundation

class GameScene: SKScene, SKPhysicsContactDelegate {
    var backgroundMusic: AVAudioPlayer?
    
    var player: Player!
    var opponent: Player!
    var ball: Ball!
    var brickList: [Brick] = [Brick]()
    
    private var leftGoal: SKNode!
    private var rightGoal: SKNode!
    
    var scoreThreshold = 1
    
    var gameOver = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    

    
    override init(size: CGSize) {
        super.init(size: size)
        backgroundColor = UIColor.blackColor()
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        
        // Setup top and bottom walls
        let topRightPoint = CGPoint(x: size.width, y: size.height)
        let topLeftPoint = CGPoint(x: 0.0, y: size.height)
        let bottomRightPoint = CGPoint(x: size.width, y: 0)
        let bottomLeftPoint = CGPoint(x: 0, y: 0)
        
        let topWall = SKPhysicsBody(edgeFromPoint: topLeftPoint, toPoint: topRightPoint)
        let bottomWall = SKPhysicsBody(edgeFromPoint: bottomLeftPoint, toPoint: bottomRightPoint)
        
        self.physicsBody = SKPhysicsBody(bodies: [topWall, bottomWall])
        self.physicsBody?.dynamic = false
        self.physicsBody?.categoryBitMask = CollisionCategoryBitmask.Wall

        
        setupPlayer()
        ball = Ball(position: CGPoint(x: self.size.width/2, y: self.size.height/2))
        addChild(ball)
        addChild(player)
        addChild(opponent)
        ball.physicsBody?.applyImpulse(CGVector(dx: 20, dy: 2))
        
        createBricks()
                
        // Setup Left and Right goals
        leftGoal = SKNode()
        leftGoal.physicsBody = SKPhysicsBody(edgeFromPoint: topLeftPoint, toPoint: bottomLeftPoint)
        leftGoal.physicsBody?.categoryBitMask = CollisionCategoryBitmask.LeftGoal
        addChild(leftGoal)
        
        rightGoal = SKNode()
        rightGoal.physicsBody = SKPhysicsBody(edgeFromPoint: topRightPoint, toPoint: bottomRightPoint)
        rightGoal.physicsBody?.categoryBitMask = CollisionCategoryBitmask.RightGoal
        addChild(rightGoal)
        
        pauseButton()
        
        // Background Music
        backgroundMusic = BackgroundMusicPlayer.create("Sounds/warrior_bgm.mp3")
    }
    
    override func didMoveToView(view: SKView) {        
        ball.updateMovingSpeed()
        backgroundMusic?.play()
        self.speed = 1
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        let touch = touches.first
        let location = touch!.locationInNode(self)
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name == "pause" {
                pauseGame()
            } else {
                movePlayer(location.y)
            }
        }
    }
    
    // Setup player, opponent and ball
    func setupPlayer() {
        player = createPlayer(CGPoint(x: 100, y: self.size.height / 2), type: GameState.sharedInstance.character)
        opponent = createPlayer(CGPoint(x: self.size.width - 100, y: self.size.height / 2), type: GameState.sharedInstance.opponentCharacter)
    }
    
    func movePlayer(y:CGFloat){
        player.moveTo(y)
    }
    
    func moveOpponent(y:CGFloat){
        opponent.moveTo(y)
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }

    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch moves */
        touchesBegan(touches, withEvent: event)
    }
    

    func createBricks() {
        var start = CGFloat(0)
        var i = 0
        while start < self.size.height {
            let playerBrick = Brick(position: CGPoint(x: Brick.brickWidth/2, y: start + Brick.brickHeight/2), health: 2,id:i)
            i++
            addChild(playerBrick)
            brickList.append(playerBrick)
            
            let opponentBrick = Brick(position: CGPoint(x: self.size.width - Brick.brickWidth/2, y: start + Brick.brickHeight/2), health: 2,id:i)
            i++
            addChild(opponentBrick)
            brickList.append(opponentBrick)
            start = start + Brick.brickHeight
        }
    }
    
    func createPlayer(position: CGPoint, type: String?) -> Player {
        if type == nil {
            return Wizard.init(position: position)
        }
        
        if type!.containsString("wizard") {
            return Wizard.init(position: position)
        } else if type!.containsString("hunter") {
            return Hunter.init(position: position)
        } else if type!.containsString("warrior") {
            return Warrior.init(position: position)
        } else {
            return Wizard.init(position: position)
        }
    }
    
    func pauseButton() {
        let button = SKShapeNode(rect: CGRect(origin: CGPoint(x: self.size.width - 42, y: self.size.height - 42), size: CGSize(width: 25, height: 40)))
        let lblMainMenu = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        button.lineWidth = CGFloat.init(0)
        lblMainMenu.fontSize = 30
        lblMainMenu.fontColor = SKColor.whiteColor()
        lblMainMenu.position = CGPoint(x: self.size.width - 30, y: self.size.height - 30)
        lblMainMenu.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblMainMenu.text = "||"
        lblMainMenu.name = "pause"
        button.name = "pause"
        button.addChild(lblMainMenu)
        addChild(button)
    }
    
    // called when the pause button is pressed
    func pauseGame() {
        let reveal = SKTransition.fadeWithDuration(0.5)
        let pauseScene = PauseScene(size: self.size)
        pauseScene.nextScene = self
        self.speed = 0
        backgroundMusic?.pause()
        self.view!.presentScene(pauseScene, transition: reveal)
    }
    
    // called when the ball collides with one of the goal
    func endGame(leftWins: Bool) {
        GameState.sharedInstance.saveState()
        self.gameOver = true
        let reveal = SKTransition.fadeWithDuration(0.5)
        
        var resultText: String
        leftWins ? (resultText = "Left Wins") : (resultText = "Right Wins")

        let endGameScene = EndGameScene(size: self.size, resultText: resultText)
        backgroundMusic?.stop()

        self.view!.presentScene(endGameScene, transition: reveal)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        ball.updateMovingSpeed()
        let nonBall = (contact.bodyA.node != ball) ? contact.bodyA.node : contact.bodyB.node
        if nonBall == leftGoal {
            print("Left Goal is shot")
            endGame(false)
        } else if nonBall == rightGoal {
            print("right Goal is shot")
            endGame(true)
        } else if let brick = nonBall as? Brick {
            brickHit(brick)
            runAction(SKAction.playSoundFileNamed("Sounds/brick_break.mp3", waitForCompletion: false))            
        } else if (nonBall?.physicsBody!.categoryBitMask == CollisionCategoryBitmask.Wall ||
                    nonBall?.physicsBody!.categoryBitMask == CollisionCategoryBitmask.Player) {
            runAction(SKAction.playSoundFileNamed("Sounds/bounce.mp3", waitForCompletion: false))
        }
    }
    
    func brickHit(b:Brick){
        b.hit()
    }
        
}



