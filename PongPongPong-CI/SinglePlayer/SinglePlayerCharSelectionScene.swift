//
//  SinglePlayerCharSelectionScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/4/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class SinglePlayerCharSelectionScene: SKScene {
    var spDelegate: SinglePlayerDelegate?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        
        // Title
        let lblTitle = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblTitle.fontSize = 60
        lblTitle.fontColor = SKColor.cyanColor()
        lblTitle.position = CGPoint(x: self.size.width / 2, y: self.size.height * 8 / 10)
        lblTitle.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblTitle.text = "Character Selection"
        addChild(lblTitle)
        
        // Warrior
        let lblWarrior = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWarrior.fontSize = 40
        lblWarrior.fontColor = SKColor.whiteColor()
        lblWarrior.position = CGPoint(x: self.size.width / 2, y: self.size.height * 6 / 10)
        lblWarrior.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblWarrior.text = "Warrior"
        lblWarrior.name = "warrior"
        addChild(lblWarrior)
        
        // Hunter
        let lblHunter = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblHunter.fontSize = 40
        lblHunter.fontColor = SKColor.whiteColor()
        lblHunter.position = CGPoint(x: self.size.width / 2, y: self.size.height * 4.5 / 10)
        lblHunter.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblHunter.text = "Hunter"
        lblHunter.name = "hunter"
        addChild(lblHunter)
        
        
        // Wizard
        let lblWizard = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWizard.fontSize = 40
        lblWizard.fontColor = SKColor.whiteColor()
        lblWizard.position = CGPoint(x: self.size.width / 2, y: self.size.height * 3 / 10)
        lblWizard.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblWizard.text = "Wizard"
        lblWizard.name = "wizard"
        addChild(lblWizard)
        
        // Back to Main Menu
        let lblMainMenu = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblMainMenu.fontSize = 30
        lblMainMenu.fontColor = SKColor.whiteColor()
        lblMainMenu.position = CGPoint(x: self.size.width / 4, y: self.size.height * 1 / 10)
        lblMainMenu.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblMainMenu.text = "Back"
        lblMainMenu.name = "back"
        addChild(lblMainMenu)
        
        // Play
        let lblPlay = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblPlay.fontSize = 30
        lblPlay.fontColor = SKColor.whiteColor()
        lblPlay.position = CGPoint(x: self.size.width * 3 / 4, y: self.size.height * 1 / 10)
        lblPlay.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblPlay.text = "Play"
        lblPlay.name = "play"
        addChild(lblPlay)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name != nil {
                runAction(SKAction.playSoundFileNamed("Sounds/click.mp3", waitForCompletion: false))
            }
            if (nodeAtTouch.name == "warrior" || nodeAtTouch.name == "hunter" || nodeAtTouch.name == "wizard") {
                clearSelection()
                (nodeAtTouch as! SKLabelNode).fontColor = SKColor.yellowColor()
                GameState.sharedInstance.character = nodeAtTouch.name
            }
            if nodeAtTouch.name == "play" {
                spDelegate!.goToGameScene()
            }
            if nodeAtTouch.name == "back" {
                spDelegate!.backToMainMenu()
            }
        }
    }
    
    override func didMoveToView(view: SKView) {
        if GameState.sharedInstance.character != nil {
            (self.childNodeWithName(GameState.sharedInstance.character!) as! SKLabelNode).fontColor = SKColor.yellowColor()
        }
    }
    
    func clearSelection() {
        (self.childNodeWithName("warrior") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("hunter") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("wizard") as! SKLabelNode).fontColor = SKColor.whiteColor()
        GameState.sharedInstance.character = nil
    }
}
