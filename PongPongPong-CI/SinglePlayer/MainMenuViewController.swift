//
//  MainMenuViewController.swift
//  PongPongPong-CI
//
//  Created by Denis Thamrin on 4/09/2015.
//  Copyright (c) 2015 Denis Thamrin. All rights reserved.
//

import UIKit
import SpriteKit

// Denis Thamrin, Delegation to seperate view controller transistion from the view
protocol MainMenuDelegate {
    func menuButtonTouched(name:String)
}

class MainMenuViewController: UIViewController, MainMenuDelegate {
    
    // MARK: - Delegate
    func menuButtonTouched(name: String) {
        self.performSegueWithIdentifier(name, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.multipleTouchEnabled = true
        
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = MainMenuScene(size: skView.bounds.size)
        scene.menuDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
        
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
