//
//  SinglePlayerGameScene.swift
//  PongPongPong-CI
//
//  Created by Peter on 16/09/2015.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class SinglePlayerGameScene: GameScene {
    
    var spDelegate: SinglePlayerDelegate?
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        super.update(currentTime)

        opponentMove()
    }
    
    func opponentMove() {
        // ball is trapped
        if ball.position.x > opponent.position.x {
            if ball.position.y < self.size.height/2 {
                opponent.moveTo(self.size.height)
            } else {
                opponent.moveTo(0)
            }
        }
        
        else {
            opponent.moveTo(ball.position.y)    
        }
    }
    
    override func pauseGame() {
        self.speed = 0
        backgroundMusic?.pause()        
        spDelegate!.goToPauseScene(self)
    }
    
    override func endGame(leftWins: Bool) {
        backgroundMusic?.stop()
        spDelegate!.goToEndGame(leftWins)
    }

}


