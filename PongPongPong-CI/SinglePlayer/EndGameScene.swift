//
//  EndGameScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 9/21/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class EndGameScene: SKScene {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(size: CGSize, resultText: String) {
        super.init(size: size)
        
        // Score
        let lblScore = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblScore.fontSize = 60
        lblScore.fontColor = SKColor.whiteColor()
        lblScore.position = CGPoint(x: self.size.width / 2, y: self.size.height * 8 / 10)
        lblScore.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblScore.text = String(format: "%d - %d", GameState.sharedInstance.playerScore, GameState.sharedInstance.aiScore)
        lblScore.name = "score"
        addChild(lblScore)
        
        // Description
        let lblDescription = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblDescription.fontSize = 30
        lblDescription.fontColor = SKColor.cyanColor()
        lblDescription.position = CGPoint(x: self.size.width / 2, y: self.size.height * 5 / 10)
        lblDescription.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblDescription.text = resultText
        addChild(lblDescription)
        
        // Back to Main Menu
        let lblMainMenu = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblMainMenu.fontSize = 30
        lblMainMenu.fontColor = SKColor.whiteColor()
        lblMainMenu.position = CGPoint(x: self.size.width / 2, y: self.size.height * 2 / 10)
        lblMainMenu.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblMainMenu.text = "Go To Main Menu"
        lblMainMenu.name = "mainmenu"
        addChild(lblMainMenu)        
    }
    
    func updateScore(scoreLeft: Int, scoreRight: Int) {
        (self.childNodeWithName("score") as! SKLabelNode).text = String(format: "%d - %d", scoreLeft, scoreRight)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name != nil && ["mainmenu"].contains(nodeAtTouch.name!) {
                runAction(SKAction.playSoundFileNamed("Sounds/click.mp3", waitForCompletion: false))
            }
            if nodeAtTouch.name == "mainmenu" {
                goToMainMenu()
            }
        }
    }
    
    func goToMainMenu() {
        let reveal = SKTransition.fadeWithDuration(0.5)
        let mainMenuScene = MainMenuScene(size: self.size)
        self.view!.presentScene(mainMenuScene, transition: reveal)
    }
}