//
//  MainMenuScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 9/23/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

@IBDesignable
class MainMenuScene: SKScene {
    var menuDelegate:MainMenuDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        // Game Title
        let lblGameTitle = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblGameTitle.fontSize = 60
        lblGameTitle.fontColor = SKColor.cyanColor()
        lblGameTitle.position = CGPoint(x: self.size.width / 2, y: self.size.height * 8 / 10)
        lblGameTitle.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblGameTitle.text = "Pong Pong Pong!"
        lblGameTitle.name = "title"
        addChild(lblGameTitle)
        
        // Single Player
        let lblSinglePlayer = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblSinglePlayer.fontSize = 30
        lblSinglePlayer.fontColor = SKColor.whiteColor()
        lblSinglePlayer.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 5 / 10)
        lblSinglePlayer.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblSinglePlayer.text = "Single Player"
        lblSinglePlayer.name = "singleplayer"
        addChild(lblSinglePlayer)
        
        // Multiplayer
        let lblMultiPlayer = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblMultiPlayer.fontSize = 30
        lblMultiPlayer.fontColor = SKColor.whiteColor()
        lblMultiPlayer.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 3.5 / 10)
        lblMultiPlayer.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblMultiPlayer.text = "Multiplayer"
        lblMultiPlayer.name = "multiplayer"
        addChild(lblMultiPlayer)
        
        // Offline Multiplayer
        let lblOfflineMultiplayer = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblOfflineMultiplayer.fontSize = 30
        lblOfflineMultiplayer.fontColor = SKColor.whiteColor()
        lblOfflineMultiplayer.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 2 / 10)
        lblOfflineMultiplayer.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblOfflineMultiplayer.text = "Offline Multiplayer"
        lblOfflineMultiplayer.name = "offlinemultiplayer"
        addChild(lblOfflineMultiplayer)
        
        // Player Rank Label
        let lblPlayerScoreTxt = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblPlayerScoreTxt.fontSize = 20
        lblPlayerScoreTxt.fontColor = SKColor.whiteColor()
        lblPlayerScoreTxt.position = CGPoint(x: self.size.width * 4.5 / 6, y: self.size.height * 5 / 10)
        lblPlayerScoreTxt.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblPlayerScoreTxt.text = "Player Score"
        lblPlayerScoreTxt.name = "playerscoretxt"
        addChild(lblPlayerScoreTxt)
        
        // AI Rank Label
        let lblAiScoreTxt = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblAiScoreTxt.fontSize = 20
        lblAiScoreTxt.fontColor = SKColor.whiteColor()
        lblAiScoreTxt.position = CGPoint(x: self.size.width * 4.5 / 6, y: self.size.height * 3 / 10)
        lblAiScoreTxt.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblAiScoreTxt.text = "AI Score"
        lblAiScoreTxt.name = "aiscoretxt"
        addChild(lblAiScoreTxt)
        
        // Player Rank
        let lblPlayerScore = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblPlayerScore.fontSize = 35
        lblPlayerScore.fontColor = SKColor.whiteColor()
        lblPlayerScore.position = CGPoint(x: self.size.width * 4.5 / 6, y: self.size.height * 4 / 10)
        lblPlayerScore.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblPlayerScore.text = String(format: "%d", GameState.sharedInstance.playerScore)
        lblPlayerScore.name = "playerscore"
        addChild(lblPlayerScore)
        
        // AI Rank
        let lblAiScore = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblAiScore.fontSize = 35
        lblAiScore.fontColor = SKColor.whiteColor()
        lblAiScore.position = CGPoint(x: self.size.width * 4.5 / 6, y: self.size.height * 2 / 10)
        lblAiScore.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblAiScore.text = String(format: "%d", GameState.sharedInstance.aiScore)
        lblAiScore.name = "aiscore"
        addChild(lblAiScore)


    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        (self.childNodeWithName("playerscore") as! SKLabelNode).text = String(format: "%d", GameState.sharedInstance.playerScore)
        (self.childNodeWithName("aiscore") as! SKLabelNode).text = String(format: "%d", GameState.sharedInstance.aiScore)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        self.menuHelper(touches)
    }
    
    func menuHelper(touches: Set<UITouch>) {
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name != nil {
                runAction(SKAction.playSoundFileNamed("Sounds/click.mp3", waitForCompletion: false))
            }
            if nodeAtTouch.name == "singleplayer" {
                self.menuDelegate!.menuButtonTouched("goToSPVC")
            } else if nodeAtTouch.name == "multiplayer" {
                self.menuDelegate!.menuButtonTouched("goToMPVC")
            } else if nodeAtTouch.name == "offlinemultiplayer" {
                self.menuDelegate!.menuButtonTouched("goToOfflineMPVC")
            }
        }
    }
}