//
//  PauseScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/4/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class PauseScene: SKScene {
    
    var nextScene: SKScene?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        // Title
        let lblTitle = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblTitle.fontSize = 60
        lblTitle.fontColor = SKColor.cyanColor()
        lblTitle.position = CGPoint(x: self.size.width / 2, y: self.size.height * 8 / 10)
        lblTitle.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblTitle.text = "Game Paused"
        addChild(lblTitle)
        
        // Resume Game
        let lblResume = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblResume.fontSize = 30
        lblResume.fontColor = SKColor.whiteColor()
        lblResume.position = CGPoint(x: self.size.width / 2, y: self.size.height * 4 / 10)
        lblResume.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblResume.text = "Resume Game"
        lblResume.name = "resume"
        addChild(lblResume)
        
        // Quit to Main Menu
        let lblMainMenu = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblMainMenu.fontSize = 30
        lblMainMenu.fontColor = SKColor.whiteColor()
        lblMainMenu.position = CGPoint(x: self.size.width / 2, y: self.size.height * 3 / 10)
        lblMainMenu.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblMainMenu.text = "Quit to Main Menu"
        lblMainMenu.name = "mainmenu"
        addChild(lblMainMenu)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name != nil && ["mainmenu", "resume"].contains(nodeAtTouch.name!) {
                runAction(SKAction.playSoundFileNamed("Sounds/click.mp3", waitForCompletion: false))
            }
            if nodeAtTouch.name == "resume" {
                self.goToNextScene()
            }
            if nodeAtTouch.name == "mainmenu" {
                self.goToMainMenu()
            }
        }
    }
    
    func goToNextScene() {
        let reveal = SKTransition.fadeWithDuration(0.5)
        self.view!.presentScene(self.nextScene!, transition: reveal)
    }
    
    func goToMainMenu() {
        let reveal = SKTransition.fadeWithDuration(0.5)
        let mainMenuScene = MainMenuScene(size: self.size)
        self.view!.presentScene(mainMenuScene, transition: reveal)
    }
    
    override func didMoveToView(view: SKView) {
        if (self.nextScene == nil) {
            self.nextScene = MainMenuScene(size: self.size)
        }
    }

}
