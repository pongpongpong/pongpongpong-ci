//
//  SinglePlayerViewController.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/18/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import UIKit
import SpriteKit

protocol SinglePlayerDelegate {
    func backToMainMenu()
    func goToGameScene()
    func goToCharSelection()
    func goToPauseScene(nextScene: SKScene)
    func goToNextScene(nextScene: SKScene)
    func goToEndGame(leftWins: Bool)
}

class SinglePlayerViewController: UIViewController, SinglePlayerDelegate {
    
    // MARK: - Delegate
    func backToMainMenu() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func goToGameScene() {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = SinglePlayerGameScene(size: skView.bounds.size)
        scene.spDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToCharSelection() {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = SinglePlayerCharSelectionScene(size: skView.bounds.size)
        scene.spDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToPauseScene(nextScene: SKScene) {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = SinglePlayerPauseScene(size: skView.bounds.size)
        scene.spDelegate = self
        scene.nextScene = nextScene
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToNextScene(nextScene: SKScene) {
        
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = nextScene
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToEndGame(leftWins: Bool) {
        leftWins ? (GameState.sharedInstance.playerScore += 1) : (GameState.sharedInstance.aiScore += 1)
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        var resultText: String
        leftWins ? (resultText = "Left Wins") : (resultText = "Right Wins")
        
        let scene = SinglePlayerEndGameScene(size: skView.bounds.size, resultText: resultText)
        scene.spDelegate = self
        scene.scaleMode = .AspectFit
        
        GameState.sharedInstance.saveState()
        skView.presentScene(scene)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.multipleTouchEnabled = true
        self.goToCharSelection()
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
