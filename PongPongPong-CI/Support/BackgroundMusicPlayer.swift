import AVFoundation

class BackgroundMusicPlayer {
    
    static func create(filename: String) -> AVAudioPlayer? {
        let url = NSBundle.mainBundle().URLForResource(
            filename, withExtension: nil)
        if (url == nil) {
            print("Could not find file: \(filename)")
            return nil
        }
        
        let error: NSError? = nil
        let backgroundMusicPlayer: AVAudioPlayer!
        do {
            backgroundMusicPlayer = try AVAudioPlayer(contentsOfURL: url!)
        } catch {
            backgroundMusicPlayer = nil
        }
        
        
        if backgroundMusicPlayer == nil {
            print("Could not create audio player: \(error!)")
            return nil
        }
        
        backgroundMusicPlayer.numberOfLoops = -1
        backgroundMusicPlayer.volume = 0.3
        backgroundMusicPlayer.prepareToPlay()
        return backgroundMusicPlayer
    }
}

