//
//  OfflineMultiplayerViewController.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/19/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import UIKit
import SpriteKit
//
//protocol SinglePlayerDelegate {
//    func backToMainMenu()
//    func goToGameScene()
//    func goToCharSelection()
//    func goToPauseScene(nextScene: SKScene)
//    func goToNextScene(nextScene: SKScene)
//    func goToEndGame(leftWins: Bool)
//}

class OfflineMultiplayerViewController: UIViewController, SinglePlayerDelegate {
    
    var scoreLeft: Int?
    var scoreRight: Int?
    
    // MARK: - Delegate
    func backToMainMenu() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func goToGameScene() {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = OfflineMultiplayerGameScene(size: skView.bounds.size)
        scene.spDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToCharSelection() {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = OfflineMultiplayerCharSelectionScene(size: skView.bounds.size)
        scene.spDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToPauseScene(nextScene: SKScene) {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = OfflineMultiplayerPauseScene(size: skView.bounds.size)
        scene.spDelegate = self
        scene.nextScene = nextScene
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToNextScene(nextScene: SKScene) {
        
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = nextScene
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToEndGame(leftWins: Bool) {
        leftWins ? (self.scoreLeft! += 1) : (self.scoreRight! += 1)
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        var resultText: String
        leftWins ? (resultText = "Left Wins") : (resultText = "Right Wins")
        
        let scene = OfflineMultiplayerEndGameScene(size: skView.bounds.size, resultText: resultText)
        scene.updateScore(self.scoreLeft!, scoreRight: self.scoreRight!)
        scene.spDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scoreLeft = 0
        self.scoreRight = 0
        self.view.multipleTouchEnabled = true
        self.goToCharSelection()
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
