//
//  OfflineMultiplayerPauseScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/19/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class OfflineMultiplayerPauseScene: PauseScene {
    
    var spDelegate: SinglePlayerDelegate?
    
    override func goToNextScene() {
        spDelegate!.goToNextScene(nextScene!)
    }
    
    override func goToMainMenu() {
        spDelegate!.backToMainMenu()
    }
}