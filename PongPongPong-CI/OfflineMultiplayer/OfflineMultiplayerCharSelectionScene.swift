//
//  OfflineMultiplayerCharSelectionScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/19/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class OfflineMultiplayerCharSelectionScene: SKScene {
    var spDelegate: SinglePlayerDelegate?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        
        // Title
        let lblTitle = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblTitle.fontSize = 60
        lblTitle.fontColor = SKColor.cyanColor()
        lblTitle.position = CGPoint(x: self.size.width / 2, y: self.size.height * 8 / 10)
        lblTitle.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblTitle.text = "Character Selection"
        addChild(lblTitle)
        
        // Warrior
        let lblWarrior = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWarrior.fontSize = 40
        lblWarrior.fontColor = SKColor.whiteColor()
        lblWarrior.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 6 / 10)
        lblWarrior.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblWarrior.text = "Warrior"
        lblWarrior.name = "warrior"
        addChild(lblWarrior)
        
        // Hunter
        let lblHunter = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblHunter.fontSize = 40
        lblHunter.fontColor = SKColor.whiteColor()
        lblHunter.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 4.5 / 10)
        lblHunter.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblHunter.text = "Hunter"
        lblHunter.name = "hunter"
        addChild(lblHunter)
        
        
        // Wizard
        let lblWizard = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWizard.fontSize = 40
        lblWizard.fontColor = SKColor.whiteColor()
        lblWizard.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 3 / 10)
        lblWizard.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblWizard.text = "Wizard"
        lblWizard.name = "wizard"
        addChild(lblWizard)
        
        // Warrior 2
        let lblWarrior2 = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWarrior2.fontSize = 40
        lblWarrior2.fontColor = SKColor.whiteColor()
        lblWarrior2.position = CGPoint(x: self.size.width * 9 / 10, y: self.size.height * 6 / 10)
        lblWarrior2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        lblWarrior2.text = "Warrior"
        lblWarrior2.name = "warrior2"
        addChild(lblWarrior2)
        
        // Hunter 2
        let lblHunter2 = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblHunter2.fontSize = 40
        lblHunter2.fontColor = SKColor.whiteColor()
        lblHunter2.position = CGPoint(x: self.size.width * 9 / 10, y: self.size.height * 4.5 / 10)
        lblHunter2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        lblHunter2.text = "Hunter"
        lblHunter2.name = "hunter2"
        addChild(lblHunter2)
        
        
        // Wizard 2
        let lblWizard2 = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWizard2.fontSize = 40
        lblWizard2.fontColor = SKColor.whiteColor()
        lblWizard2.position = CGPoint(x: self.size.width * 9 / 10, y: self.size.height * 3 / 10)
        lblWizard2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        lblWizard2.text = "Wizard"
        lblWizard2.name = "wizard2"
        addChild(lblWizard2)
        
        // Back to Main Menu
        let lblMainMenu = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblMainMenu.fontSize = 30
        lblMainMenu.fontColor = SKColor.whiteColor()
        lblMainMenu.position = CGPoint(x: self.size.width / 4, y: self.size.height * 1 / 10)
        lblMainMenu.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblMainMenu.text = "Back"
        lblMainMenu.name = "back"
        addChild(lblMainMenu)
        
        // Play
        let lblPlay = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblPlay.fontSize = 30
        lblPlay.fontColor = SKColor.whiteColor()
        lblPlay.position = CGPoint(x: self.size.width * 3 / 4, y: self.size.height * 1 / 10)
        lblPlay.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblPlay.text = "Play"
        lblPlay.name = "play"
        addChild(lblPlay)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            
            if nodeAtTouch.name != nil {
                runAction(SKAction.playSoundFileNamed("Sounds/click.mp3", waitForCompletion: false))
            }
            
            // update selection for player 1
            if (nodeAtTouch.name == "warrior" || nodeAtTouch.name == "hunter" || nodeAtTouch.name == "wizard") {
                clearSelection()
                (nodeAtTouch as! SKLabelNode).fontColor = SKColor.yellowColor()
                GameState.sharedInstance.character = nodeAtTouch.name
            }
            
            // update selection for player 2
            if (nodeAtTouch.name == "warrior2" || nodeAtTouch.name == "hunter2" || nodeAtTouch.name == "wizard2") {
                clearSelection2()
                (nodeAtTouch as! SKLabelNode).fontColor = SKColor.yellowColor()
                GameState.sharedInstance.opponentCharacter = nodeAtTouch.name
            }
            if nodeAtTouch.name == "play" {
                spDelegate!.goToGameScene()
            }
            if nodeAtTouch.name == "back" {
                spDelegate!.backToMainMenu()
            }
        }
    }
    
    override func didMoveToView(view: SKView) {
        if GameState.sharedInstance.character != nil {
            (self.childNodeWithName(GameState.sharedInstance.character!) as! SKLabelNode).fontColor = SKColor.yellowColor()
        }
    }
    
    // clear the character selection for player 1
    func clearSelection() {
        (self.childNodeWithName("warrior") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("hunter") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("wizard") as! SKLabelNode).fontColor = SKColor.whiteColor()
        GameState.sharedInstance.character = nil
    }
    
    // clear the character selection for player 2
    func clearSelection2() {
        (self.childNodeWithName("warrior2") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("hunter2") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("wizard2") as! SKLabelNode).fontColor = SKColor.whiteColor()
        GameState.sharedInstance.opponentCharacter = nil
    }
}
