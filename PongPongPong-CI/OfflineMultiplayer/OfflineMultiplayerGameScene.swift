//
//  OfflineMultiplayerGameScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/13/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class OfflineMultiplayerGameScene: GameScene {
    
    var spDelegate: SinglePlayerDelegate?
    
    override func pauseGame() {
        self.speed = 0
        backgroundMusic?.pause()
        spDelegate!.goToPauseScene(self)
    }
    
    override func endGame(leftWins: Bool) {
        backgroundMusic?.stop()
        spDelegate!.goToEndGame(leftWins)
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name == "pause" {
                pauseGame()
            } else if location.x > self.size.width / 2 {
                opponent.moveTo(location.y)
            } else {
                player.moveTo(location.y)
            }
        }
    }
}


