//
//  OfflineMultiplayerEndGameScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/19/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class OfflineMultiplayerEndGameScene: EndGameScene {
    
    var spDelegate: SinglePlayerDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize, resultText: String) {
        super.init(size: size, resultText: resultText)
        
        // Back to Main Menu
        let lblPlayAgain = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblPlayAgain.fontSize = 30
        lblPlayAgain.fontColor = SKColor.whiteColor()
        lblPlayAgain.position = CGPoint(x: self.size.width / 2, y: self.size.height * 3 / 10)
        lblPlayAgain.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblPlayAgain.text = "Play Again"
        lblPlayAgain.name = "playagain"
        addChild(lblPlayAgain)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        super.touchesBegan(touches, withEvent: event)
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            if nodeAtTouch.name == "playagain" {
                playAgain()
            }
        }
    }
    
    func playAgain() {
        spDelegate?.goToCharSelection()
    }
    
    override func goToMainMenu() {
        spDelegate!.backToMainMenu()
    }
}