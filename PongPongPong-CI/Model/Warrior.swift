import SpriteKit

class Warrior : Player {
    static let speed = 0.5
    static let color = UIColor.redColor()
    static let size = CGSize(width: 20, height: 150)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(position: CGPoint) {
        super.init(position: position, maxSpeed: Warrior.speed, color: Warrior.color, size: Warrior.size)
    }    

}
