import SpriteKit

class Hunter : Player {
    static let speed = 3.0
    static let color = UIColor.greenColor()
    static let size = CGSize(width: 20, height: 40)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(position: CGPoint) {
        super.init(position: position, maxSpeed: Hunter.speed, color: Hunter.color, size: Hunter.size)
    }
    
}
