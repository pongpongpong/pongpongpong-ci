import SpriteKit

class Wizard : Player {    
    static let speed = 1.0
    static let color = UIColor.whiteColor()
    static let size = CGSize(width: 20, height: 100)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(position: CGPoint) {
        super.init(position: position, maxSpeed: Wizard.speed, color: Wizard.color, size: Wizard.size)
    }
    
}
