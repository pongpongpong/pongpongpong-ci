import SpriteKit

class Brick : SKNode {
    static let brickWidth = CGFloat(15)
    static let brickHeight = CGFloat(35)
    
    var id:Int!
    
    var maxHealth: Int!
    var health: Int! {
        didSet{
            if health == 0 {
                NSLog("Brick destroyed")
                removeFromParent()
            } else if (health != maxHealth) {
                NSLog("Brick damaged")
                removeAllChildren()
                let sprite = SKSpriteNode(imageNamed: "Brick-cracked.png")
                sprite.size = CGSize(width: Brick.brickWidth, height: Brick.brickHeight)
                addChild(sprite)
            }
        }
    }
    var type: String!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(position: CGPoint, health: Int,id : Int) {
        super.init()
        self.health = health
        self.maxHealth = health
        self.id = id
      
        let sprite = SKSpriteNode(imageNamed: "Brick.png")
        sprite.size = CGSize(width: Brick.brickWidth, height: Brick.brickHeight)
        
        self.position = position
        physicsBody = SKPhysicsBody(rectangleOfSize: sprite.size)
        physicsBody?.dynamic = true
        physicsBody?.categoryBitMask = CollisionCategoryBitmask.Brick
        physicsBody?.collisionBitMask = CollisionCategoryBitmask.Brick
        physicsBody?.allowsRotation = false
        
        addChild(sprite)
    }
    
    func hit() {
        health! -= 1
    }

}
