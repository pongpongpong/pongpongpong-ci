//
//  GameState.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 9/21/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import Foundation

class GameState {
    
    var playerScore: Int
    var aiScore: Int
    
    var character: String?
    var opponentCharacter: String?
    
    class var sharedInstance: GameState {
        struct Singleton {
            static let instance = GameState()
        }
        return Singleton.instance
    }
    
    init() {
        // Init
        playerScore = 0
        aiScore = 0
        
        // Load game state
        let defaults = NSUserDefaults.standardUserDefaults()
        
        playerScore = defaults.integerForKey("playerScore")
        aiScore = defaults.integerForKey("aiScore")
        character = defaults.stringForKey("character")
        opponentCharacter = defaults.stringForKey("opponentCharacter")
    }
    
    func saveState() {
        // Store in user defaults
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(playerScore, forKey: "playerScore")
        defaults.setInteger(aiScore, forKey: "aiScore")
        defaults.setObject(character, forKey: "character")
        defaults.setObject(opponentCharacter, forKey: "opponentCharacter")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
