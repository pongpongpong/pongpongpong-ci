struct CollisionCategoryBitmask {
    static let None: UInt32      = 0b0
    static let Player: UInt32    = 0b1
    static let Ball: UInt32      = 0b10
    static let Wall: UInt32      = 0b100
    static let LeftGoal: UInt32  = 0b1000
    static let RightGoal: UInt32 = 0b10000
    static let Brick: UInt32     = 0b100000
    static let All: UInt32       = UInt32.max
}