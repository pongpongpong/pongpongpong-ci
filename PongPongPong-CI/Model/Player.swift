import SpriteKit

class Player : SKNode {
    
    var maxSpeed: Double?

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    init(position: CGPoint, maxSpeed: Double) {
        super.init()
        let sprite = SKSpriteNode(color: UIColor.whiteColor(), size: CGSize(width: 20, height: 100))
        self.position = position
        self.maxSpeed = 0.005 / maxSpeed
        physicsBody = SKPhysicsBody(rectangleOfSize: sprite.size)
        physicsBody?.dynamic = true
        physicsBody?.categoryBitMask = CollisionCategoryBitmask.Player
        physicsBody?.collisionBitMask = CollisionCategoryBitmask.Wall
        physicsBody?.allowsRotation = false
        
        addChild(sprite)
    }    
    
    init(position: CGPoint, maxSpeed: Double, color: UIColor, size: CGSize) {
        super.init()
        let sprite = SKSpriteNode(color: color, size: size)
        self.position = position
        self.maxSpeed = 0.005 / maxSpeed
        physicsBody = SKPhysicsBody(rectangleOfSize: sprite.size)
        physicsBody?.dynamic = true
        physicsBody?.categoryBitMask = CollisionCategoryBitmask.Player
        physicsBody?.collisionBitMask = CollisionCategoryBitmask.Wall
        physicsBody?.allowsRotation = false
        
        addChild(sprite)
    }
    
    func moveTo(yPos: CGFloat) {
        let move = SKAction.moveTo(CGPoint(x: position.x, y: yPos), duration: abs(Double(position.y - yPos) * self.maxSpeed!))
        runAction(move)
    }

}
