import SpriteKit

class Ball : SKNode {
    
    var movingSpeed = CGFloat(400)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(position: CGPoint) {
        super.init()
        
        let ballShape = SKShapeNode(circleOfRadius: 20 )
        ballShape.strokeColor = SKColor.blackColor()
        ballShape.glowWidth = 1.0
        ballShape.fillColor = SKColor.orangeColor()
        addChild(ballShape)
        
        physicsBody = SKPhysicsBody(circleOfRadius: 20)
        physicsBody?.dynamic = true
        physicsBody?.categoryBitMask = CollisionCategoryBitmask.Ball
        physicsBody?.contactTestBitMask = CollisionCategoryBitmask.All
        physicsBody?.collisionBitMask = CollisionCategoryBitmask.All
        
        physicsBody?.restitution = 1.0
        physicsBody?.friction = 0
        physicsBody?.linearDamping = 0.0
        physicsBody?.angularDamping = 0.0
        
        self.position = position
    }
    
    func changeSpeed(speed: CGFloat) {
        movingSpeed = speed
        let v = physicsBody!.velocity
        if v.dx == 0 && v.dy == 0 {
            return
        }
        let scale = speed/(sqrt(v.dx*v.dx + v.dy*v.dy))
        physicsBody?.velocity = CGVector(dx: v.dx * scale, dy: v.dy * scale)
    }
    
    func updateMovingSpeed() {
        changeSpeed(movingSpeed)
    }
}
