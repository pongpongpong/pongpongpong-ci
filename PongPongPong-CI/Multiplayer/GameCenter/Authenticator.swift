//
//  SharedGCHelper.swift
//  A singleton class to help authenticate local player using NSnotification
//  PongPongPong-CI
//  Refactor to auntheticator.swift
//  Created by Denis Thamrin on 9/09/2015.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//


import GameKit

// List of notification that game center helper notifies
// Controller should listen to this notification and react accordingly.
struct AuthenticatorNotificationName {
    // Controller should show the game center screen
    static let showAvc = "SHOW GAMECENTER AUTHENTICATION"
    static let playerAuthenticated = "AUTHENTICATION SUCCEED"
    static let playerNotAuthenticated = "AUTHENTICATION FAILED"
}

class Authenticator{
    //Singleton class, get its instance from this property
    static let sharedInstance = Authenticator()
    
    private init (){
        
    }

    
// MARK: - Authentication
    // Is local player authenticated ?
    var playerAuthenticated:Bool {
        get{
            return localPlayer.authenticated
        }
    }
    
    var localPlayer:GKLocalPlayer! = GKLocalPlayer.localPlayer()

    // Post notifications every time game center controller need to be displayed
    private let center:NSNotificationCenter = NSNotificationCenter.defaultCenter()
    

    
    //Last error that was caught in game center
    private var lastError:NSError?{
        willSet{
            NSLog(newValue!.userInfo.description)
            NSLog("error")
        }
    }
    
// MARK - API
    
    var authenticationViewController:UIViewController? {
        didSet {
            let notificiation = NSNotification(name: AuthenticatorNotificationName.showAvc, object: oldValue)
            center.postNotification(notificiation)
        }
    }
    
    func authenticateLocalPlayer() {
        NSLog("Authenticating local player")
        localPlayer.authenticateHandler = {
            (let gcvc:UIViewController?,let error:NSError?) -> Void in
                NSLog("Handler authenticating")
            
            if let _ = error {
                self.lastError = error
            }
            
            if let _ = gcvc {
                self.authenticationViewController = gcvc
            } else if self.playerAuthenticated {
                let notificiation = NSNotification(name: AuthenticatorNotificationName.playerAuthenticated, object: nil)
                self.center.postNotification(notificiation)
            } else if !self.playerAuthenticated {
                let notificiation = NSNotification(name: AuthenticatorNotificationName.playerNotAuthenticated, object: nil)
                self.center.postNotification(notificiation)
            }
            
        
        }
    }
 

    

}
