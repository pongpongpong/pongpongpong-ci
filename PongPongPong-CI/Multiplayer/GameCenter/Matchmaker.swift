//
//  Matchmaker.swift
//  PongPongPong-CI
//  Class to match make based on p2p
//  Created by Denis Thamrin on 22/09/2015.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import GameKit

protocol FailureDelegate {
    func returnToMainMenu()
}

class Matchmaker:NSObject,GKMatchmakerViewControllerDelegate{
    private let minPlayers:Int = 2
    private let maxPlayers:Int = 2
    //Store the match to receive and send data
    
    private var multiplayerNetworking:MultiplayerNetworking!
    var failureDelegate:FailureDelegate?
    
    
    
// MARK - GKMatchmakervc Delegate
    
    func matchmakerViewControllerWasCancelled(viewController: GKMatchmakerViewController) {
        viewController.dismissViewControllerAnimated(true, completion:
            { () in
                self.failureDelegate!.returnToMainMenu()
        })
        NSLog("Match cancelled")
      
    }
    
    
    func matchmakerViewController(viewController: GKMatchmakerViewController, didFailWithError error: NSError) {
        viewController.dismissViewControllerAnimated(true, completion:
            { () in
                self.failureDelegate!.returnToMainMenu()
        })
        NSLog("Match fail to find with error !")
    }
    
    func matchmakerViewController(viewController: GKMatchmakerViewController, didFindMatch match: GKMatch) {
        match.delegate = multiplayerNetworking
        self.multiplayerNetworking.match = match
        
//        NSLog("Match is ready to start")
//
//                //Choose best player, if apple fails to find one, choose based on ascending playerId
//                match.chooseBestHostingPlayerWithCompletionHandler(
//                    { ( player:GKPlayer?) -> Void in
//
//          
//                            NSLog("Manually resolve host")
//                            self.multiplayerNetworking.resolveHostManually()
//                        
//        
//                        viewController.dismissViewControllerAnimated(true, completion: nil)
//                        
//                    }
//                )
        
        NSLog("Manually resolve host")
        self.multiplayerNetworking.resolveHostManually()
        
        viewController.dismissViewControllerAnimated(true, completion: nil)

        
        //Hybrid of letting apple or self resolve host. Causes bug as at some point of time, each device may choose different technique
//        //Choose best player, if apple fails to find one, choose based on ascending playerId
//        match.chooseBestHostingPlayerWithCompletionHandler(
//            { ( player:GKPlayer?) -> Void in
//                
//                if player != nil {
//                    NSLog("Automatic resolve hosy by apple")
//                    self.multiplayerNetworking.hostingPlayer = player
//
//                } else {
//                    NSLog("Manually resolve host")
//                    self.multiplayerNetworking.resolveHostManually()
//                }
//            
//                viewController.dismissViewControllerAnimated(true, completion: nil)
//                
//            }
//        )
//        
//        self.failureDelegate!.returnToMainMenu()
        
        
       

        
    }

// MARK - API
    
    func findMatch(vc:UIViewController,delegate:MultiplayerNetworking){
        let match = GKMatchRequest()
        match.minPlayers = minPlayers
        match.maxPlayers = maxPlayers
        multiplayerNetworking = delegate
        
        let mmvc = GKMatchmakerViewController(matchRequest: match)!
        mmvc.matchmakerDelegate = self
        
        vc.dismissViewControllerAnimated(false, completion: nil)
        vc.presentViewController(mmvc, animated: true, completion: nil)
       
    }
    
    
    

    
}
