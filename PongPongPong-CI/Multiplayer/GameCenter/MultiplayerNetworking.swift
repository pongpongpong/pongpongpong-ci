//
//  MultiplayerNetworking.swift
//  PongPongPong-CI
//  Multiplayer Networking
//  Created by Denis Thamrin on 23/09/2015.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import GameKit

//VC must implement this to receive network data
protocol MultiplayerNetworkingDelegate {
    func matchStarted()
    func matchEnded()
    func receivedBallFromNetwork(v:CGVector,position:CGPoint)
    func receivedPlayerLocationFromNetwork(y:CGFloat)
    func receivedBrickHit(brickHealth:Int,brickId:Int)
    
}


//Message send between devices
enum MessageType:Int {
    case Ball,PlayerLocation,BrickHit,RandomNumber
}

struct Message {
    let messageType:MessageType
}

struct MessageBall{
    let message: Message
    let position: CGPoint
    let velocity: CGVector
}

struct MessagePlayerLocation{
    let message: Message
    let y:CGFloat
}

struct MessageBrickHit {
    let message:Message
    let brickHealth:Int
    let brickId:Int
}

struct MessageRandomNumber{
    let message:Message
    let randomNumber:Int
    
}



class MultiplayerNetworking:NSObject,GKMatchDelegate{
    var localRandomNumber:Int {
        get {
            let playerID:String! = localPlayer.playerID!
            var i = 0
            var x = 0
            for _ in playerID.characters {
                x += Int(Array(playerID.unicodeScalars)[i].value)
                i++
            }
            return x
        }
    }
    
    var delegate:MultiplayerNetworkingDelegate?
    var match:GKMatch?
    
    var hostingPlayer:GKPlayer? {
        didSet{
            isHost = hostingPlayer?.playerID == localPlayer.playerID
        }
    }
    private var localPlayer = Authenticator.sharedInstance.localPlayer
    var matchStarted:Bool = false
    var isHost:Bool? {
        didSet {
            if oldValue == nil{
                NSLog("setting Host")
                delegate!.matchStarted()
                resolveHostManually()
            }
        }
    }
    

// MARK: - API
    func sendBall(b:Ball){
        NSLog(" \(hostingPlayer?.playerID) vs \(localPlayer.playerID) ")

        if self.isHost! {
            NSLog("Sending Ball")
            var message = MessageBall(message: Message(messageType: MessageType.Ball), position:b.position,velocity:b.physicsBody!.velocity)
            let data = NSData(bytes: &message, length: sizeof(MessageBall))
            sendData(data)
            
            
        }
    }
    
    func sendBrickHit(b:Brick){
        if self.isHost! {
            NSLog("Sending brick hit")
            var message = MessageBrickHit(message: Message(messageType: MessageType.BrickHit), brickHealth:b.health, brickId: b.id!)
            let data = NSData(bytes: &message, length: sizeof(MessageBrickHit))
            sendData(data)
        }
    }
    
    
    func sendPlayerLocation(y:CGFloat){
        var message = MessagePlayerLocation(message: Message(messageType: MessageType.PlayerLocation), y: y)
        let data = NSData(bytes: &message, length: sizeof(MessagePlayerLocation))
        sendData(data)
    }
    
    private func sendData(data:NSData){
        _ = try! self.match!.sendDataToAllPlayers(data, withDataMode: GKMatchSendDataMode.Unreliable)
        
    }
    
    func resolveHostManually(){

       
        // can be null
        
        var message = MessageRandomNumber(message: Message(messageType: MessageType.RandomNumber), randomNumber:localRandomNumber)
        let data = NSData(bytes: &message, length: sizeof(MessageRandomNumber))
        sendData(data)
    }
    
    
// MARK: - Match Delegate
    func match(match: GKMatch, didReceiveData data: NSData, fromRemotePlayer player: GKPlayer) {
        NSLog("Received Data !")
        let message = UnsafePointer<Message>(data.bytes).memory
        if let _ = isHost {
            if message.messageType == MessageType.Ball {
                NSLog("Ball Location data received")
                let ball = UnsafePointer<MessageBall>(data.bytes).memory
                self.delegate!.receivedBallFromNetwork(ball.velocity,position:ball.position)
                
            } else if message.messageType == MessageType.PlayerLocation {
                NSLog("Player Location data received")
                let playerLocation = UnsafePointer<MessagePlayerLocation>(data.bytes).memory
                self.delegate!.receivedPlayerLocationFromNetwork(playerLocation.y)
            } else if message.messageType == MessageType.BrickHit{
                let data = UnsafePointer<MessageBrickHit>(data.bytes).memory
                NSLog("BrickHit received \(data.brickId)")
                self.delegate!.receivedBrickHit(data.brickHealth, brickId: data.brickId)
            }
        }else if message.messageType == MessageType.RandomNumber{
            NSLog("random received")
            
            let data = UnsafePointer<MessageRandomNumber>(data.bytes).memory
            if data.randomNumber > self.localRandomNumber{
                NSLog("You are host")
                isHost = true
            } else if data.randomNumber <= self.localRandomNumber  {
                NSLog("You are not host")
                isHost = false
            }
        }

        
    }
    

 
    func match(match: GKMatch, didFailWithError error: NSError?) {
        delegate?.matchEnded()
        NSLog("Match failed with error %s", (error?.localizedDescription)! )
    }
    
    

    
    func match(match: GKMatch, player: GKPlayer, didChangeConnectionState state: GKPlayerConnectionState) {
        //NSLog("Not implemented yet,connection changed state %s", state.hashValue)
        switch(state){
        case .StateConnected:

            NSLog("Player connected")
        case .StateDisconnected:
            NSLog("Player disconnected")
            delegate?.matchEnded()
        case .StateUnknown:
            NSLog("State unknown ?")
        }
        
    
    }


}
