//
//  MultiplayerEndGameScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/20/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class MultiplayerEndGameScene: EndGameScene {
    
    var mpvcDelegate: MultiplayerControllerDelegate?
    
    override func goToMainMenu() {
        mpvcDelegate!.backToMainMenu()
    }
}