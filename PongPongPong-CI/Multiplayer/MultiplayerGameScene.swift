//
//  MultiplayerGameScene.swift
//  PongPongPong-CI
//
//  Created by Denis Thamrin on 11/10/2015.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//


import SpriteKit

protocol MultiplayerSceneNetworkingDelegate {
    func sendPlayerLocation(y:CGFloat)
    func sendBall(b:Ball)
    func sendBrick(b:Brick)
    func isHost() -> Bool
}

class MultiplayerGameScene : GameScene {
    var networkDelegate: MultiplayerSceneNetworkingDelegate?
    var mpvcDelegate: MultiplayerControllerDelegate?
  
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(size: CGSize,delegate mpvcD:MultiplayerControllerDelegate) {
        self.mpvcDelegate = mpvcD
        super.init(size:size)

        sendMessage()
        let _ = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: "sendMessage", userInfo: nil, repeats: true)
    }
    
    func sendMessage() {
        networkDelegate?.sendBall(super.ball)
    }
    
    func swapSide(){
        let tmp = super.player
        super.player = super.opponent
        super.opponent = tmp
    }
    
    func receiveBrickHit(id:Int,health:Int){
        for b in super.brickList{
            if b.id! == id {
                NSLog("Updated brick health")
                b.health! = health
            } else {
                NSLog("Match not found ballid:\(b.id!) networkid:\(id)")
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        super.update(currentTime)
    }
 
    override func brickHit(b: Brick) {
        if networkDelegate!.isHost(){
            super.brickHit(b)
            networkDelegate?.sendBrick(b)
            
        }
        
    }
    
    override func movePlayer(y:CGFloat){
        player.moveTo(y)
        networkDelegate?.sendPlayerLocation(y)
    }
 
    func updateBallFromNetwork(v:CGVector,p:CGPoint){
        super.ball.position = p
        super.ball.physicsBody?.velocity = v
    }
    
    
    func remoteOponentMove(y:CGFloat) {
        super.moveOpponent(y)
    }
    
    // disable the pause button
    override func pauseButton() {
    }
    
    override func endGame(leftWins: Bool) {
        backgroundMusic?.stop()
        mpvcDelegate!.goToEndGame(leftWins)
    }
    
    override func setupPlayer() {
        player = createPlayer(CGPoint(x: 100, y: self.size.height / 2), type: mpvcDelegate!.hostChar)
        opponent = createPlayer(CGPoint(x: self.size.width - 100, y: self.size.height / 2), type: mpvcDelegate!.opponentChar)
    }
}