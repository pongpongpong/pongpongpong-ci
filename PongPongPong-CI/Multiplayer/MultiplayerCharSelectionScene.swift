//
//  MultiplayerCharSelectionScene.swift
//  PongPongPong-CI
//
//  Created by Adrian Sutanahadi on 10/20/15.
//  Copyright © 2015 Denis Thamrin. All rights reserved.
//

import SpriteKit

class MultiplayerCharSelectionScene: SKScene {
    var mpvcDelegate: MultiplayerControllerDelegate?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        
        // Title
        let lblTitle = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblTitle.fontSize = 60
        lblTitle.fontColor = SKColor.cyanColor()
        lblTitle.position = CGPoint(x: self.size.width / 2, y: self.size.height * 8 / 10)
        lblTitle.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblTitle.text = "Character Selection"
        addChild(lblTitle)
        
        // Warrior
        let lblWarrior = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWarrior.fontSize = 40
        lblWarrior.fontColor = SKColor.whiteColor()
        lblWarrior.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 6 / 10)
        lblWarrior.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblWarrior.text = "Warrior"
        lblWarrior.name = "warrior"
        addChild(lblWarrior)
        
        // Hunter
        let lblHunter = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblHunter.fontSize = 40
        lblHunter.fontColor = SKColor.whiteColor()
        lblHunter.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 4.5 / 10)
        lblHunter.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblHunter.text = "Hunter"
        lblHunter.name = "hunter"
        addChild(lblHunter)
        
        
        // Wizard
        let lblWizard = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWizard.fontSize = 40
        lblWizard.fontColor = SKColor.whiteColor()
        lblWizard.position = CGPoint(x: self.size.width * 1 / 10, y: self.size.height * 3 / 10)
        lblWizard.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        lblWizard.text = "Wizard"
        lblWizard.name = "wizard"
        addChild(lblWizard)
        
        // Warrior 2
        let lblWarrior2 = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWarrior2.fontSize = 40
        lblWarrior2.fontColor = SKColor.whiteColor()
        lblWarrior2.position = CGPoint(x: self.size.width * 9 / 10, y: self.size.height * 6 / 10)
        lblWarrior2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        lblWarrior2.text = "Warrior"
        lblWarrior2.name = "warrior2"
        addChild(lblWarrior2)
        
        // Hunter 2
        let lblHunter2 = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblHunter2.fontSize = 40
        lblHunter2.fontColor = SKColor.whiteColor()
        lblHunter2.position = CGPoint(x: self.size.width * 9 / 10, y: self.size.height * 4.5 / 10)
        lblHunter2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        lblHunter2.text = "Hunter"
        lblHunter2.name = "hunter2"
        addChild(lblHunter2)
        
        
        // Wizard 2
        let lblWizard2 = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblWizard2.fontSize = 40
        lblWizard2.fontColor = SKColor.whiteColor()
        lblWizard2.position = CGPoint(x: self.size.width * 9 / 10, y: self.size.height * 3 / 10)
        lblWizard2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        lblWizard2.text = "Wizard"
        lblWizard2.name = "wizard2"
        addChild(lblWizard2)
        
        // Play
        let lblPlay = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        lblPlay.fontSize = 30
        lblPlay.fontColor = SKColor.whiteColor()
        lblPlay.position = CGPoint(x: self.size.width / 2, y: 60)
        lblPlay.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        lblPlay.text = "Play"
        lblPlay.name = "play"
        addChild(lblPlay)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let nodeAtTouch = self.nodeAtPoint(touch.locationInNode(self))
            
            // update selection for player 1
            if (nodeAtTouch.name == "warrior" || nodeAtTouch.name == "hunter" || nodeAtTouch.name == "wizard") {
                clearSelection()
                (nodeAtTouch as! SKLabelNode).fontColor = SKColor.yellowColor()
                mpvcDelegate?.hostChar = nodeAtTouch.name
            }
            
            // update selection for player 2
            if (nodeAtTouch.name == "warrior2" || nodeAtTouch.name == "hunter2" || nodeAtTouch.name == "wizard2") {
                clearSelection2()
                (nodeAtTouch as! SKLabelNode).fontColor = SKColor.yellowColor()
                mpvcDelegate!.opponentChar = nodeAtTouch.name
            }
            if nodeAtTouch.name == "play" {
                mpvcDelegate!.goToGameScene()
            }
        }
    }
    
    // clear the character selection for player 1 (host)
    func clearSelection() {
        (self.childNodeWithName("warrior") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("hunter") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("wizard") as! SKLabelNode).fontColor = SKColor.whiteColor()
        mpvcDelegate?.hostChar = nil
    }
    
    // clear the character selection for player 2 (opponent)
    func clearSelection2() {
        (self.childNodeWithName("warrior2") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("hunter2") as! SKLabelNode).fontColor = SKColor.whiteColor()
        (self.childNodeWithName("wizard2") as! SKLabelNode).fontColor = SKColor.whiteColor()
        mpvcDelegate?.opponentChar = nil
    }
}
