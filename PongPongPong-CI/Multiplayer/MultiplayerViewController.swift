//
//  MultiplayerViewController.swift
//  
//
//  Created by Denis Thamrin on 14/09/2015.
//
//

import GameKit

protocol MultiplayerControllerDelegate {
    var hostChar: String? { get set }
    var opponentChar: String? { get set }
    func backToMainMenu()
    func goToGameScene()
    func goToCharSelection()
    func goToEndGame(leftWins: Bool)
}

class MultiplayerViewController: UIViewController, MultiplayerNetworkingDelegate, MultiplayerSceneNetworkingDelegate, MultiplayerControllerDelegate,FailureDelegate {
    let gch = Authenticator.sharedInstance
    var matchmaker = Matchmaker()
    var multiplayerScene:MultiplayerGameScene?
    
    let multiplayerNetworking = MultiplayerNetworking()
    
    var hostChar: String?
    var opponentChar: String?
    
// MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        // Do any additional setup after loading the view.
    }

    func setup (){
        let center = NSNotificationCenter.defaultCenter()
        matchmaker.failureDelegate = self
        center.addObserver(self, selector: "showAuthenticationViewController", name: AuthenticatorNotificationName.showAvc, object: nil)
        center.addObserver(self, selector: "findMatch", name: AuthenticatorNotificationName.playerAuthenticated, object: nil)
        if (Authenticator.sharedInstance.playerAuthenticated)
        {
            findMatch()
        } else {
            Authenticator.sharedInstance.authenticateLocalPlayer()
        }
        multiplayerNetworking.delegate = self
    }

// MARK: - MultiplayerNetworking Delegate
    
    func matchStarted() {

        self.goToGameScene()
    }
    

    func matchEnded() {
//        multiplayerScene!.endGame()
        let leftWins = true;
        self.goToEndGame(leftWins)
        NSLog("Match ended")
    }
    
    func receivedBallFromNetwork(v:CGVector,position:CGPoint) {
        multiplayerScene?.updateBallFromNetwork(v,p: position)
    }
    func receivedPlayerLocationFromNetwork(y:CGFloat){
        multiplayerScene?.moveOpponent(y)
    }
    
    // If you received it then you are an opponent
    // only host sends the data
    func receivedBrickHit(brickHealth: Int, brickId: Int) {
        multiplayerScene?.receiveBrickHit(brickId ,health: brickHealth)
    }

    func isHost() -> Bool {
        return multiplayerNetworking.isHost!
    }
    
// MARK: - Matchmaking
    func findMatch(){
        matchmaker.findMatch(self,delegate:multiplayerNetworking)
    }
    
    
// MARK: - Authentication
    
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        NSLog("finished")
    }
    
    func showAuthenticationViewController () {
        
        if let avc = gch.authenticationViewController{
            self.view.window!.rootViewController!.presentViewController(avc, animated: true, completion: nil )
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
// MARK: - MultiplayerSceneDelegate
    func sendPlayerLocation(y: CGFloat) {
        multiplayerNetworking.sendPlayerLocation(y)
    }
    
    func sendBall(b:Ball) {
        multiplayerNetworking.sendBall(b)
    }
    
    func sendBrick(b:Brick){
        multiplayerNetworking.sendBrickHit(b)
    }
    
// MARK: - Navigation Delegate
    func backToMainMenu() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func goToGameScene() {
        let skView = view! as! SKView
        skView.multipleTouchEnabled = false
        
        let reveal = SKTransition.fadeWithDuration(0.5)
        multiplayerScene = MultiplayerGameScene(size: skView.bounds.size,delegate: self)
        multiplayerScene!.networkDelegate = self
        
        if !multiplayerNetworking.isHost! {
            multiplayerScene?.swapSide()
        }
        
        skView.presentScene(multiplayerScene!, transition: reveal)
        
        NSLog("Match started controller")

    }
    
    func goToCharSelection() {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        let scene = MultiplayerCharSelectionScene(size: skView.bounds.size)
        scene.mpvcDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
    }
    
    func goToEndGame(leftWins: Bool) {
        let skView = self.view as! SKView
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        var resultText: String
        leftWins ? (resultText = "Left Wins") : (resultText = "Right Wins")
        
        let scene = MultiplayerEndGameScene(size: skView.bounds.size, resultText: resultText)
        scene.mpvcDelegate = self
        scene.scaleMode = .AspectFit
        
        skView.presentScene(scene)
        
    }
    
// MARK: - Failure Delegate
    func returnToMainMenu() {
        backToMainMenu()
    }
}





